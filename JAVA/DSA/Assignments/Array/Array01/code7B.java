import java.util.*;
class LeadersInArray{
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter size of array : ");
    int N = sc.nextInt();
    int []arr = new int[N];
    System.out.println("Enter Elements : ");
    for(int i =0;i<N;i++) {
      arr[i] = sc.nextInt();
    }
    int max = Integer.MIN_VALUE;
    for(int i =N-1;i>0;i--) {
        if(max < arr[i]) {
	  max = arr[i];
	  System.out.println(max+",");
	}
    }
    
  }
}
