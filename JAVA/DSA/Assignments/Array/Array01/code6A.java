import java.util.*;
class ProductExceptItself{
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter size of array : ");
    int N = sc.nextInt();
    int []arr = new int[N];
    System.out.println("Enter Elements : ");
    for(int i =0;i<N;i++) {
      arr[i] = sc.nextInt();
    }
    int []prodArr = new int[N];

    for(int i =0;i<N;i++) {
      int product = 1;
      for(int j =0;j<N;j++) {
         if(j!=i) {
	   product *= arr[j];
      }
      prodArr[i] = product;
    }
    }
    for(int i =0;i<N;i++) {
      System.out.print(prodArr[i]+",");
    }
    
  }
}
