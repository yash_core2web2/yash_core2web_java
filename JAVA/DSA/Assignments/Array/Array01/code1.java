import java.util.*;
class MinAndMaxSum {
  public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
     System.out.println("Enter size of array : ");
     int N = sc.nextInt();
     int []arr = new int[N];
     System.out.println("Enter array elements : ");
     for(int i =0;i<N;i++) {
       arr[i] = sc.nextInt();
     }
     int max = Integer.MIN_VALUE;
     int min = Integer.MAX_VALUE;
     for(int i =0;i<N;i++) {
       if(max < arr[i]) {
         max = arr[i];
       }
       if(min > arr[i]) {
         min = arr[i];
       }
     }
     long sum = min + max;
     System.out.println(sum);
  }
}
