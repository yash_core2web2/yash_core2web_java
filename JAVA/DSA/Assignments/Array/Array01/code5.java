import java.util.*;
class PrefixSum{
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter size of array : ");
    int N = sc.nextInt();
    int []arr = new int[N];
    System.out.println("Enter Elements : ");
    for(int i =0;i<N;i++) {
      arr[i] = sc.nextInt();
    }

    int []prefArr = new int[N];
    prefArr[0] = arr[0];
    for(int i =1;i<N;i++) {
      prefArr[i] = prefArr[i-1] + arr[i];
    }
    for(int i =0;i<N;i++) {
      System.out.print(prefArr[i] +",");
    }
  }
}
