import java.util.*;
class LeadersInArray{
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter size of array : ");
    int N = sc.nextInt();
    int []arr = new int[N];
    System.out.println("Enter Elements : ");
    for(int i =0;i<N;i++) {
      arr[i] = sc.nextInt();
    }
    for(int i =0;i<N-1;i++) {
      int flag = 1;
      for(int j = i;j<N;j++) {
        if(arr[i]<arr[j]) {
	  flag = 0;
	}
      }
	if(flag == 1) {
	  System.out.print(arr[i]+",");
	}
    }
    System.out.print(arr[N-1]);
  }
}
