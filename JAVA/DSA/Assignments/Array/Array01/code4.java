import java.util.*;
class TimeToEquality {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter size of array : ");
    int N = sc.nextInt();
    int []arr = new int[N];
    System.out.println("Enter elements : ");
    for(int i =0;i<N;i++) {
       arr[i] = sc.nextInt();
    }
    int max = Integer.MIN_VALUE;
    for(int i =0;i<N;i++) {
      if(max < arr[i]) {
        max = arr[i];
      }
    }
    int time = 0;
    for(int i =0;i<N;i++) {
      if(arr[i]<max) {
         time =time + (max - arr[i]);
      }
    }
    System.out.println(time);
  }
}
