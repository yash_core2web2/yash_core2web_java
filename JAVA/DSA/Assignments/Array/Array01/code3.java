import java.util.*;
class RangeSum {
  public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
     System.out.println("Enter size of array : ");
     int N = sc.nextInt();
     int []arr = new int[N];
     System.out.println("Enter array elements : ");
     for(int i =0;i<N;i++) {
       arr[i] = sc.nextInt();
     }
     int [][]dArr = new int[2][2];
     System.out.println("Enter array elements : ");
     for(int i =0;i<2;i++) {
      for(int j =0;j<2;j++) {
       dArr[i][j] = sc.nextInt();
     }
     }
     int []sum = new int[2];
     int []preArr = new int[N];
     preArr[0] = arr[0];
     for(int i =1;i<N;i++) {
       preArr[i] = preArr[i-1] + arr[i];
     }
     for(int i =0;i<2;i++) {
	 if(dArr[i][0]==0) {
	    sum[i] = preArr[dArr[i][1]];
	 }else {
            sum[i] = preArr[dArr[i][1]] - preArr[dArr[i][0]-1];
	 }
     }
     for(int i = 0;i<2;i++) {
       System.out.print(sum[i] + ",");	
     }
  }
}
