import java.util.*;
class Occurance {
  public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
     System.out.println("Enter size of array : ");
     int N = sc.nextInt();
     int []arr = new int[N];
     System.out.println("Enter array elements : ");
     for(int i =0;i<N;i++) {
       arr[i] = sc.nextInt();
     }
     System.out.println("Enter key : ");
     int B = sc.nextInt();
     int count = 0;
     for(int i =0;i<N;i++) {
       if(B == arr[i]) {
         count++;
       }
     }
     System.out.println(count);
  }
}
