import java.util.*;
class ProductExceptItself{
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter size of array : ");
    int N = sc.nextInt();
    int []arr = new int[N];
    System.out.println("Enter Elements : ");
    for(int i =0;i<N;i++) {
      arr[i] = sc.nextInt();
    }
    int []prodArr = new int[N];
    prodArr[N-1] = arr[N-1];
    for(int i =N-2;i>=0;i--) {
       prodArr[i] = prodArr[i+1] * arr[i];
    }

    int product = 1;
    prodArr[0] = prodArr[1];
    for(int i =1;i<N;i++) {
     product = product * arr[i-1];
     if(i<N-1) {	   
      prodArr[i] = prodArr[i+1] * product;
     }else {
      prodArr[i] = product;
     }
    }
    for(int i =0;i<N;i++) {
      System.out.print(prodArr[i]+",");
    }
    
  }
}
