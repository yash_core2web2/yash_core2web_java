/////////////BETTER////////////////////
//    TC = O(N^2)     SC = O(1)
import java.util.*;
class LargestSubOfO1 {
  public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
     int N = sc.nextInt();
     int []arr = new int[N];
     System.out.println("Enter array elements : ");
     for(int i =0;i<N;i++) {
        arr[i] = sc.nextInt();
     }
     int maxLen = 0;
     for(int i =0;i<N;i++) {
       int count0 = 0;
       int count1 = 0;
       for(int j = i;j<N;j++) {
	     if(arr[j] == 0) {
	      count0++;
	     }else {
	      count1++;
	     }
	     if(count0 == count1) {
	        if(maxLen < j-(i-1)) {
		  maxLen = j-(i-1);
		}
	     }
       }
     }
     System.out.println(maxLen);
  }
}
