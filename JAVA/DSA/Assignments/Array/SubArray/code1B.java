  //////////////CarryForward////////////////
  //TC = O(N^2)  SC = O(1)

import java.util.*;
class GivenSum {
  public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      System.out.println("Enter size of array : ");
      int N = sc.nextInt();
      int []arr = new int[N];
      System.out.println("Enter elements of array : ");
      for(int i =0;i<N;i++) {
        arr[i] = sc.nextInt();
      }
      System.out.println("Enter Sum : ");
      int keySum = sc.nextInt();
      int flag = 0;
      int start = -1;
      int end = -1;
      for(int i = 0;i<N;i++) {
	int sum = 0;
        for(int j = i;j<N;j++) {
	   sum = sum + arr[j];
	   if(sum == keySum) {
	      start = i+1;
	      end = j+1;
	      flag = 1;
	      break;
	   }
	}
	if(flag == 1) {
	  break;
	}
      }
      if(flag == 1) {
        System.out.print((start) + " " + (end));
      }else {
        System.out.println("-1");
      }
  }
}
