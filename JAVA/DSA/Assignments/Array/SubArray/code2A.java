/////////////BRUTFORCE////////////////////
//    TC = O(N)     SC = O(N)
import java.util.*;
class EquiIndex {
  public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
     int N = sc.nextInt();
     int []arr = new int[N];
     System.out.println("Enter array elements : ");
     for(int i =0;i<N;i++) {
        arr[i] = sc.nextInt();
     }
     int []rightSum = new int[N];
     rightSum[N-1] = arr[N-1];
     for(int i = N-2;i>=0;i--) {
        rightSum[i] = rightSum[i+1] + arr[i];
     }
     int sum = 0;
     int output = -1;
    if(N>1) {
     for(int i = 0;i<N;i++) {
       if(i==0) {
          if(sum == rightSum[i+1]) {
             output = i;
	     break;
	  }
       }else {
          sum = sum + arr[i-1];
	  if(sum == (rightSum[i]-arr[i])) {
	    output = i+1;
	    break;
	  }
       }
     }
     System.out.println(output);
    }else {
       System.out.println(1);
    }
  }
}
