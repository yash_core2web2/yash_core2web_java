/////////////OPTIMIZED////////////////////
//    TC = O(N)     SC = O(1)
import java.util.*;
class EquiIndex {
  public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
     int N = sc.nextInt();
     int []arr = new int[N];
     System.out.println("Enter array elements : ");
     for(int i =0;i<N;i++) {
        arr[i] = sc.nextInt();
     }
     int rightSum = 0;
     for(int i = N-1;i>0;i--) {
       rightSum += arr[i];
     } 
     int output = -1;
     int sum  = 0;
     if(N==1) {
        System.out.println("1");
     }else {
        for(int i = 0;i<N;i++) {
	  if(i==0) {
	     if(0==rightSum) {
	        output = i+1;
		break;
	     }
	  }else {
	     sum = sum + arr[i-1];
	     rightSum = rightSum - arr[i];
	     if(sum == rightSum){
	        output = i+1;
		break;
	     }
	  }
	}
        System.out.println(output);
     }
  }
}
