/////////////BRUTFORCE////////////////////
//    TC = O(N^3)     SC = O(1)
import java.util.*;
class SubArrWithSum {
  public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
     int N = sc.nextInt();
     int []arr = new int[N];
     System.out.println("Enter array elements : ");
     for(int i =0;i<N;i++) {
        arr[i] = sc.nextInt();
     }
     int flag = 0;
     for(int i =0;i<N;i++) {
        for(int j =i;j<N;j++) {
	   int sum = 0;
	   for(int k =i;k<=j;k++) {
	      sum = sum + arr[k];
	   }
	   if(sum == 0) {
	      flag = 1;
	      break;
	   }
	}
	if(flag == 1) {
	   break;
	}
     }
     if(flag == 1) {
        System.out.println("YES");
     }else {
        System.out.println("NO");
     }
  }
}
