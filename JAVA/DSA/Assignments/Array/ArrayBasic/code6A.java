/////////////BRUTFORCE////////
class RangeCheck {
  public static void main(String[] args) {
     int N = 7;
     int A = 2;
     int B = 6;
     int []arr = new int[]{1,4,5,2,7,8,3};
     int reqCount = B-A+1;
     int count = 0;
     for(int i =A;i<=B;i++) {
	int flag = 0;
       for(int j = 0;j<arr.length;j++) {
         if(arr[j] == i) {
	    count++;
	    flag = 1;
	    break;
	 }
       }
       if(flag == 0) {
           break;
       }
     }
     System.out.println(count);
     if(count == reqCount) {
       System.out.println("Yes");
     }else {
       System.out.println("No");
     }
  }
}
                            //TC = O((B-A+1)*N)
                            //SC = O(1)
