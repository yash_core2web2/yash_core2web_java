import java.util.*;
class MinMax {
  public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      int size = sc.nextInt();
      int []arr = new int[size];
      for(int i =0;i<arr.length;i++) {
        arr[i] = sc.nextInt();
      }
      int max = Integer.MIN_VALUE;
      int min = Integer.MAX_VALUE;
      for(int i =0;i<arr.length;i++) {
         if(max < arr[i]) {
	   max = arr[i];
	 }
	 if(min > arr[i]) {
	   min = arr[i];
	 }
      }
      System.out.println("min : "+min);
      System.out.println("max : "+max);
  }
}


    // TC = O(N)
    // SC = O(1)
