import java.util.*;
class Replace0 {
  public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
     int num = sc.nextInt();
     int temp = num;
     int rem = 0;
     int output = 0;
     int count = 1;
     while(temp > 0) {
        rem = temp % 10;
	if(rem == 0) {
	   output = output + (5*count);
	}else {
	   output = output + (rem*count);
	}
	count = count * 10;
	temp /= 10;
     }
     System.out.println(output);
  }
}

