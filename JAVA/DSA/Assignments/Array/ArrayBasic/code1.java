import java.util.*;
class Search {
  public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      int size = sc.nextInt();
      int []arr = new int[size];
      for(int i =0;i<arr.length;i++) {
        arr[i] = sc.nextInt();
      }
      int key = sc.nextInt();
      int flag = 0;
      for(int i =0;i<arr.length;i++) {
         if(key == arr[i]) {
	    System.out.println(i);
	    flag = 1;
	    break;
	 }
      }
      if(flag == 0) {
        System.out.println("Element not found");
      }
  }
}


    // TC = O(N)
    // SC = O(1)
