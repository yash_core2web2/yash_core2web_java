class RangeCheck {

  static boolean check(int[] arr,int N,int A,int B) {
      int range = B-A+1;
      if(range > N) {
        return false;
      }
      for(int i =0;i<arr.length;i++) {
         if(Math.abs(arr[i])>=A && Math.abs(arr[i])<=B) {
	    int z = Math.abs(arr[i]) - A;
	    if(arr[z] > 0) {
	      arr[z] = arr[z] * (-1);
	    }
	 }
      }
      int count = 0;
      for(int i =0;i<arr.length;i++) {
        if(arr[i] < 0) {
	  count++;
	}
      }
      if(count == range) {
        return true;
      }else {
        return false;
      }
  }

  public static void main(String[] args) {
     int N = 7;
     int A = 2;
     int B = 5;
     int []arr = new int[]{1,4,5,2,7,8,3};
     if(check(arr,N,A,B)) {
        System.out.println("Yes");
     }else {
        System.out.println("No");
     }

  }
}
