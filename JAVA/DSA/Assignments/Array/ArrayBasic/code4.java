import java.util.*;
class ModuloMultiply {
  static long multiplication(int []arr,int size) {
     long product = 1;
     long MODULO = 1000000007;  //  10^9+7
     for(int i =0;i<size;i++) {
        product = (product*arr[i]) % MODULO;
     }
     return product;
  } 
	
  public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
     System.out.println("Enter number of test cases : ");
     int tests = sc.nextInt();
     while(tests>0) {
        System.out.println("Enter size of array : ");
	int size = sc.nextInt();
	int []arr = new int[size];
	for(int i =0;i<size;i++) {
	  arr[i] = sc.nextInt();
	}
	System.out.println(multiplication(arr,size));
	tests--;
     }
  }
}
