import java.util.*;
class Largest {
  public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      int size = sc.nextInt();
      int []arr = new int[size];
      for(int i =0;i<arr.length;i++) {
        arr[i] = sc.nextInt();
      }
      int largest = Integer.MIN_VALUE;
      for(int i =0;i<arr.length;i++) {
          if(arr[i] > largest) {
	     largest  = arr[i];
	  }
      }
      System.out.println(largest);
      
  }
}


    // TC = O(N)
    // SC = O(1)
