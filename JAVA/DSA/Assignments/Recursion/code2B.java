class NaturalNums {
  static void printNums(int num) {
     if(num == 0) {
        return;
     }
     System.out.println(num);
     printNums(--num);
  }
  public static void main(String[] args) {
     printNums(10);
  }
}
