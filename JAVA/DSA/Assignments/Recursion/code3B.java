class Sum {
  static int summation(int num) {
     if(num == 0) {
        return 0;
     }
     return (num + summation(--num));
  }
  public static void main(String[] args) {
     System.out.println(summation(10));
  }
}
