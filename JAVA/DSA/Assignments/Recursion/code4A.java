class Length {
  static int numLength(int num) {
     int count = 0;
     for(;num>0;num/=10) {
       count++;
     }
     return count;
  }
  public static void main(String[] args) {
     System.out.println(numLength(531845));
  }
}
