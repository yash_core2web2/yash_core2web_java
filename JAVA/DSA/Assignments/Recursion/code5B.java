class CheckSum0 {
  
  public static void main(String[] args) {
     int N = 5;	  
     int []arr = new int[]{4,2,3,1,6};
     int []preArr = new int[5];
     preArr[0] = arr[0];
     for(int i =1;i<N;i++) {
        preArr[i] = preArr[i-1] + arr[i];
     }
     int flag = 0;
     for(int i =0;i<N;i++) {
        int count = 0;
	for(int j =0;j<N;j++) {
	   if(preArr[i] == preArr[j]) {
	     count++;
	   }
	}
	if(count>=2){
	  flag = 1;
	  break;
	}
     }
     if(flag == 1) {
        System.out.println("Yes");
     }else{
        System.out.println("No");
     }
  }
}
