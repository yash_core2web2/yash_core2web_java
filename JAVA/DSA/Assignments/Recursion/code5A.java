class PrimeOrNot {
  static boolean check(int num) {
     int count = 2;
     for(int i =2;i<=(num/2);i++) {
        if(num%i==0) {
	   count++;
	}
     }
     if(count>2) {
        return false;
     }else {
        return true;
     }
  }
  public static void main(String[] args) {
     System.out.println(check(31));
  }
}
