class Length {
  static int numLength(int num) {
     if(num == 0) {
       return 0;
     }
     return numLength(num/10) + 1;
  }
  public static void main(String[] args) {
     System.out.println(numLength(531845));
  }
}
