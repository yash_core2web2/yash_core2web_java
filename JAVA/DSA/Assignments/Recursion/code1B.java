class PrintNum {
  static void printNums(int num) {
     if(num == 0) {
        return;
     }
     printNums(num-1);
     System.out.println(num);
  }
  public static void main(String[] args) {
     printNums(10);
  }
}
