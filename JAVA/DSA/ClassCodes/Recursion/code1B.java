import java.util.*;
class RecursionDemo {
 
  static void PrintNum(int num) {
       
      if(num == 0) {
        return;
      }else {
        PrintNum(num-1);
	System.out.println(num);
      }
  }

  public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
     int num = sc.nextInt();
     PrintNum(num);
  }
}
