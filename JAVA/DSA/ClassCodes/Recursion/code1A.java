import java.util.*;
class Demo {
  static int start = 1;
  static void PrintNum(int num) {
       
       if(start<=num) {
         System.out.println(start++);
	 PrintNum(num);
       }else {
         return;
       }
  }

  public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
     int num = sc.nextInt();
     PrintNum(num);
  }
}
