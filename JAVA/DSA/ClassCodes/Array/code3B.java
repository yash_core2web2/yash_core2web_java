class ArrayDemo {
  static void reverse(int []arr,int n) {
    for(int i =0;i<n/2;i++) {
      int temp = arr[i];
      arr[i] = arr[n-i-1];
      arr[n-i-1] =  temp;
    }
    for(int i =0;i<n;i++) {
      System.out.print(arr[i]+" ");
    }
  }
  public static void main(String[] args) {
    int n1 = 4;
    int n2 = 5;
    int[] arr1 = new int[]{1,2,3,4};
    int[] arr2 = new int[]{1,2,3,4,5};
    reverse(arr1,n1);
    reverse(arr2,n2);

  }
}
