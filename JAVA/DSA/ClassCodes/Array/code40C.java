import java.util.*;
class SlidingWindow {
    public static void main(String[] args) {
	  Scanner sc = new Scanner(System.in);
          int []arr = new int[]{-3,4,-2,5,3,-2,8,2,1,4};
	  int size = sc.nextInt();
	  int []pArr = new int[arr.length];
	  pArr[0] = arr[0];
	  for(int i =1;i<arr.length;i++) {
	    pArr[i] = pArr[i-1] + arr[i];
	  }
	  int maxSum = Integer.MIN_VALUE;
	  int sum = 0;
	  for(int i =0;i<= (size-1);i++) {
	     sum = sum + arr[i];
	  } 
	  int start = 1;
	  int end = size;
	  while(end < arr.length) {
              if(sum>maxSum) {
	         maxSum = sum;
	      }
	      sum = sum - arr[start-1] + arr[end];
	      start++;
	      end++;
	  }
	  System.out.println(maxSum);
    }
}


                          ///  USING SLIDING WINDOW ///
