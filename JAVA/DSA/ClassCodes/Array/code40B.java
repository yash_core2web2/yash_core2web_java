import java.util.*;
class MaxSumSubArray {
    public static void main(String[] args) {
	  Scanner sc = new Scanner(System.in);
          int []arr = new int[]{-3,4,-2,5,3,-2,8,2,1,4};
	  int size = sc.nextInt();
	  int []pArr = new int[arr.length];
	  pArr[0] = arr[0];
	  for(int i =1;i<arr.length;i++) {
	    pArr[i] = pArr[i-1] + arr[i];
	  }
	  int maxSum = Integer.MIN_VALUE;
	  int start = 0;
	  int end = size-1;
	  while(end < arr.length) {
             int sum = 0;
	     if(start == 0) {
	        sum = pArr[0];
             }else {
	        sum = pArr[end] - pArr[start-1];
	     }
	     if(sum>maxSum) {
	       maxSum = sum;
	     }
	     start++;
	     end++;
	  }
	  System.out.println(maxSum);
    }
}


                          ///  USING PREFIX SUM ///
