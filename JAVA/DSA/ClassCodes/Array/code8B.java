class LeftMax {
    public static void main(String[] args) {
       int[] arr = new int[]{-3,6,2,4,5,2,8,-9,3,1};
       int[] reqArr = new int[arr.length];
       reqArr[0] = arr[0];
       for(int i = 1;i<arr.length;i++) {
          if(arr[i]>reqArr[i-1]) {
	     reqArr[i] = arr[i];
	  }else {
	     reqArr[i] = reqArr[i-1];
	  }
       }
       
       for(int i =0;i<reqArr.length;i++) {
         System.out.print(reqArr[i]+" ");
       }
    }
}
