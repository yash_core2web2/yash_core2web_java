class ArrayDemo {
   static int counter(int[] arr,int n) {
       int max = Integer.MIN_VALUE;
       int count = 0;
       for(int i  =0;i<n;i++) {
           if(arr[i]>max) {
	      max = arr[i];
	   }
       }
       for(int i =0;i<n;i++) {
         if(arr[i]!=max) {
	   count++;
	 }
       }
       return count;
   }
   public static void main(String[] args) {
      int N = 10;
      int[] arr = new int[]{2,5,1,4,8,0,8,1,3,8};
      System.out.println(counter(arr,N));
   }
}
