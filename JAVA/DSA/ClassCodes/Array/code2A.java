class ArrayDemo {
   static int counter(int[] arr,int n,int k) {
      int count = 0;
      for(int i =0;i<n;i++) {
        for(int j =0;j<n;j++) {
	   if((arr[i]+arr[j]==k) && i!=j) {
	     count++;
	   }
	}
      }
      return count;
   }
   public static void main(String[] args) {
       int n = 10;
       int[] arr = new int[]{3,5,2,1,-3,7,8,15,6,13};
       int k = 10;
       System.out.println(counter(arr,n,k));
   }
}
