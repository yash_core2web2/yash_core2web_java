class ArrayDemo {
    static int max(int[] arr,int n) {
       int max = Integer.MIN_VALUE;
       for(int i =0;i<n;i++) {
         if(max<arr[i]){
	    max = arr[i];
	 }
       }
       int max_2 = 0;
       if(arr[0] == max) {
         max_2 = arr[1];
       }else {
         max_2 = arr[0];
       }
       for(int i =0;i<n;i++) {
         if(max_2<arr[i] && arr[i]<max) {
	   max_2 = arr[i];
	 }
       }
       return max_2;
    }
    public static void main(String[] args) {
       int n1 = 4;
       int n2 = 5;
       int[] arr1 = new int[]{5,7,3,9};
       int[] arr2 = new int[]{9,5,7,3,6};
       System.out.println(max(arr1,n1));
       System.out.println(max(arr2,n2));
    }
}
