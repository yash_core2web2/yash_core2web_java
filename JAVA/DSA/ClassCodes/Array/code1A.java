class ArrayDemo {
   static int counter(int[] arr,int n) {
       int count = 0;
       for(int i  =0;i<n;i++) {
         for(int j =0;j<n;j++) {
	    if(arr[i]<arr[j]) {
	       count++;
	       break;
	    }
	 }
       }
       return count;
   }
   public static void main(String[] args) {
      int N = 10;
      int[] arr = new int[]{2,5,1,4,8,0,8,1,3,8};
      System.out.println(counter(arr,N));
   }
}
