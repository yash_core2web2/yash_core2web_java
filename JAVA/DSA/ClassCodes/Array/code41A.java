class MinSwaps {
   public static void main(String[] args) {
      int []arr = new int[]{1,12,17,3,10,14,10,5};
      int B = 8;
      int count = 0;
      for(int i =0;i<arr.length;i++) {
         if(arr[i]<=8) {
	   count++;
	 }
      }
      int start = 0;
      int end = count-1;
      int maxCount = 0;
      while(end<arr.length) {
	 int counter = 0;
         for(int i =start;i<=end;i++) {
		 if(arr[i]<=8) {
		    counter++;
		 }
	 }
	 if(maxCount<count) {
	   maxCount = counter;
	 }
	 end++;
	 start++;
      }
      System.out.println("Min Swaps Required : " + (count-maxCount));
   }
}
