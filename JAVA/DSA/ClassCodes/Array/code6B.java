import java.util.*;
class RangeSum {
  public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
     int[] arr = new int[]{2,5,3,11,7,9};
     for (int i = 1;i<=3;i++){
        System.out.println("Start : ");
        int start = sc.nextInt();
        System.out.println("End : ");
        int end = sc.nextInt();
        int sum = 0;
        for(int j =start;j<=end;j++) {
           sum += arr[j];
        }
        System.out.println(sum);
     }
  }
}
