class TransposeRectangular{
   public static void main(String[] args) {
     int [][]arr = new int[][]{{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20}};
     int [][]tArr = new int[arr[0].length][arr.length];
     for(int i =0;i<arr.length;i++) {
       for(int j =0;j<arr[0].length;j++) {
	       tArr[j][i] = arr[i][j];
       }
     }
     for(int i =0;i<tArr.length;i++) {
        for(int j=0;j<tArr[0].length;j++) {
	  System.out.print(tArr[i][j]+"\t");
	}
        System.out.println();
     }
     
  }
}
