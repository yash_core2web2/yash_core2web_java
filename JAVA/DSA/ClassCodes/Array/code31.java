class RightToLeftDiag {
  public static void main(String[] args) {
    int [][]arr = new int[][]{{1,2,3,4,5,6},{7,8,9,10,11,12},{13,14,15,16,17,18}};
    for(int i = arr[0].length-1;i>=0;i--) {
       int row = 0;
       int col = i;
       while(row<arr.length && col>=0) {
          System.out.print(arr[row][col]+" ");
	  col--;
	  row++;
       }
       System.out.println();
    }
  }
}
