class LeftMax {
    public static void main(String[] args) {
       int[] arr = new int[]{-3,6,2,4,5,2,8,-9,3,1};
       int[] reqArr = new int[arr.length];
       reqArr[0] = arr[0];
       int max = Integer.MIN_VALUE;
       for(int i =1;i<arr.length;i++) {
         for(int j =0;j<=i;j++) {
	   if(arr[j]>max) {
	      max = arr[j];
	   }
	 }
	 reqArr[i] = max;
       }
       for(int i =0;i<reqArr.length;i++) {
         System.out.print(reqArr[i]+" ");
       }
    }
}
