class RightMax {
   static int[] right(int[] arr,int N) {
      int max = Integer.MIN_VALUE;
      for(int i=N-1;i>=0;i--) {
          if(arr[i]>max) {
	     max = arr[i];
	  }
	  arr[i] = max;
      }
      return arr;
   }

   public static void main(String[] args) {
      int[] arr = new int[]{-3,6,2,4,5,2,8,-9,3,1};
      int[] req = right(arr,arr.length);
      for(int i =0;i<req.length;i++) {
        System.out.print(req[i]+" ");
      }
   }
}
