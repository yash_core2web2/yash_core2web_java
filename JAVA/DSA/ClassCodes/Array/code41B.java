class MinSwapsSlidingWApproach {
   public static void main(String[] args) {
      int []arr = new int[]{1,12,17,10,14,3,10,5};
      int B = 8;
      int size = 0;
      for(int i =0;i<arr.length;i++) {
         if(arr[i]<=B) {
	   size++;
	 }
      }
      int count = 0;
      for(int i =0;i<size;i++) {
         if(arr[i]<=B) {
	    count++;
	 }
      }
      int maxCount = count;
      int start = 1;
      int end = size;
      while(end<arr.length) {
	 if(arr[start-1]<=B) {
	    count--;
	 }
	 if(arr[end]<=B) {
	    count++;
	 }
	 if(count >maxCount) {
	    maxCount = count;
	 }
	 end++;
	 start++;
      }
      System.out.println("Min Swaps Required : " + (size-maxCount));
   }
}
