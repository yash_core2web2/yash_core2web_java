class Kadane_Algo {
  public static void main(String[] args) {
    //int []arr = new int[]{-2,1,-3,4,-1,2,1,-5,4};
    int []arr = new int[]{10,-5,-4,-1,2};
    int maxSum = Integer.MIN_VALUE;
    int sum = 0;
    int start = -1;
    int end = -1;
    int x = -1;
    for(int i =0;i<arr.length;i++) {
       if(sum == 0) {
         x = i;
       }
       sum = sum + arr[i];
       if(sum>maxSum) {
         maxSum = sum;
	 start = x;
	 end = i;
       }
       if(sum < 0) {
          sum = 0;
       }
    }
    for(int i = start;i<=end;i++) {
      System.out.println(arr[i]);
    }
    System.out.println(maxSum);
  }
}
