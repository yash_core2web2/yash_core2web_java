import java.util.*; 
class PrefixSum {
   static int[] psArray(int[] arr) {
     int[] psArr = new int[arr.length];
     psArr[0] = arr[0];
     for(int i =1;i<arr.length;i++){
       psArr[i] = psArr[i-1] + arr[i];
     }
     return psArr;
   }
   public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);
       int[] arr = new int[]{-3,6,2,4,5,2,8,-9,3,1};
       int[] preSumArr = psArray(arr);
       for(int i = 0;i<arr.length;i++) {
         System.out.print(preSumArr[i]+" ");
       }
	 System.out.println();
       for(int i = 1;i<3;i++) {
         int start = sc.nextInt();
         int end = sc.nextInt();
	 if(start == 0) {
	    int sum = preSumArr[end];
	    System.out.println(sum);
	 }else {
	    int sum = preSumArr[end] - preSumArr[start-1];
	    System.out.println(sum);
	 }
       }
   }
}
