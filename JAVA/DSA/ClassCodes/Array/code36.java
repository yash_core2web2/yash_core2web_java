class PrintAllDiagonals{
   public static void main(String[] args) {
     int [][]arr = new int[][]{{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20}};
     //All Diagonals from leftTop to RightBottom
     for(int j=0;j<arr[0].length;j++) {
        int row = 0;
	int col = j;
	while(row<arr.length && col>=0) {
	  System.out.print(arr[row][col]+"\t");
	  row++;
	  col--;
	}
	System.out.println();
     }
     for(int i =1;i<arr.length;i++) {
        int row = i;
	int col = arr[0].length-1;
	while(row<arr.length) {
	  System.out.print(arr[row][col]+"\t");
	  row++;
          col--;
	}
	System.out.println();
     }
  }
}
