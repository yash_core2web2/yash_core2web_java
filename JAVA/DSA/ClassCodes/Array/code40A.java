import java.util.*;
class MaxSumSubArray {
    public static void main(String[] args) {
	  Scanner sc = new Scanner(System.in);
          int []arr = new int[]{-3,4,-2,5,3,-2,8,2,1,4};
	  int size = sc.nextInt();
	  int maxSum = Integer.MIN_VALUE;
	  int start = 0;
	  int end = size-1;
	  while(end < arr.length) {
             int sum = 0;
	     for(int i = start;i<=end;i++) {
	        sum = sum + arr[i];
	     }
	     if(sum>maxSum) {
	       maxSum = sum;
	     }
	     start++;
	     end++;
	  }
	  System.out.println(maxSum);
    }
}
