class Rotation {

  static int[] Rotation(int[] arr,int N,int B) {
        int[] arr2 = new int[N];
	int i = 0;
	for(int j = B;j<N;j++) {
	   arr2[j] = arr[i];
	   i++;
	}
	for(int j =0;j<B;j++) {
	  arr2[j] = arr[i];
	  i++;
	}
	return arr2;
  }


  public static void main(String[] args) {
    int[] arr = new int[]{1,2,3,4};
    int[] arrRotate = Rotation(arr,arr.length,2);
    for(int i =0;i<arrRotate.length;i++) {
      System.out.print(arrRotate[i] + " ");
    }
  }
}
