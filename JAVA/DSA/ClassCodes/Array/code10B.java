class Equilibrium {
  public static void main(String[] args) {
      int[] arr = new int[]{-7,1,5,2,-4,3,0};
      int totalSum = 0;
      for(int i =0;i<arr.length;i++) {
        totalSum += arr[i];
      }

      int leftSum = 0;
      int flag = 0;
      int equindex = Integer.MAX_VALUE;
      for(int i =0;i<arr.length;i++) {
          if(leftSum == (totalSum-arr[i]-leftSum)) {
	    flag = 1;
	    if(equindex>i) {
	       equindex = i;
	    }
	  }
	  leftSum += arr[i];
      }
      if(flag == 0) {
        System.out.println("-1");
      }else {
        System.out.println("Equilibrium index : "+equindex);
      }
  }
}
