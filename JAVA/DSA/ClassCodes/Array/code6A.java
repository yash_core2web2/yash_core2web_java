import java.util.*;
class RangeSum {
  public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
     int[] arr = new int[]{2,5,3,11,7,9};
     int start = sc.nextInt();
     int end = sc.nextInt();
     int sum = 0;
     for(int i =start;i<=end;i++) {
       sum += arr[i];
     }
     System.out.println(sum);
  }
}
