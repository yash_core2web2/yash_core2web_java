class SubArray {
  public static void main(String[] args) {
    //int[] arr = new int[]{1,2,3,1,3,4,6,4,6,3};
    int[] arr = new int[]{2,2,2,2,2};
    int max = Integer.MIN_VALUE;
    int min = Integer.MAX_VALUE;
    for(int i =0;i<arr.length;i++) {
      if(max<arr[i]) {
         max = arr[i];
      }
      if(min>arr[i]) {
         min = arr[i];
      }
    }

    int length = Integer.MAX_VALUE;
    for(int i = 0;i<arr.length;i++) {
       if(arr[i]==max) {
         for(int j = i;j<arr.length;j++) {
	    if(arr[j]==min) {
	       if(length>(j-i+1)) {
	           length = (j-i+1);
	       }
	    }
	 }
       }else if(arr[i]==min) {
         for(int j = i;j<arr.length;j++) {
	    if(arr[j]==max) {
	       if(length>(j-i+1)) {
	           length = (j-i+1);
	       }
	   }
       }
    }
  }

   System.out.println("smallest length of subArray : "+length);
}
}
