
class SumSubArrayPrefix {
  public static void main(String[] args) {
     int[] arr = new int[]{2,4,1,3};
     int[] preArr = new int[arr.length];
     preArr[0] = arr[0];
     for(int i =1;i<arr.length;i++) {
       preArr[i] = preArr[i-1] + arr[i];
     }
     for(int i = 0;i<arr.length;i++) {
        for(int j =i;j<arr.length;j++) {
	   int sum =  0;
	   if(i==0) {
	     sum = preArr[j];
	   }else {
	     sum = preArr[j] - preArr[i-1];
	   }
	   System.out.println(sum);
	}
     }
  }
}
