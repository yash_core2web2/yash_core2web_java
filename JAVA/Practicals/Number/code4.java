
import java.io.*;
class Factorial {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter num : ");
    int num = Integer.parseInt(br.readLine());
    int temp = num;
    int fact = 1;
    while(temp>=1) {
      fact = fact*temp;
      temp--;
    }
    System.out.println("Factorial of "+num+" is "+fact);
  }
}
