
import java.io.*;
class ReverseDemo {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter num : ");
    int num = Integer.parseInt(br.readLine());
    int temp = num;
    int rem;
    int rev = 0;
    while(temp>0) {
	 rem = temp % 10;
	 rev = rev*10 + rem;
	 temp /= 10;
    }
    System.out.println("Reverse of "+num+" is "+rev);
  }
}
