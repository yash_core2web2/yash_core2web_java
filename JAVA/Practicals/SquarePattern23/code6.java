import java.io.*;
class Pattern6 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter number of rows : ");
    int row = Integer.parseInt(br.readLine());
    for(int i =1;i<=row;i++) {
      int num = row*row;
      for(int j =1;j<=row;j++) {
         if(i%2==1) {
	   System.out.print(num-- + "\t");
	 }else {
	   if(j%2==1) {
	     System.out.print(num + "\t");
	   }else {
	     num -= 5;
	     System.out.print(num + "\t");
	   }
	 }
      }
      System.out.println();
    }
  }
}
