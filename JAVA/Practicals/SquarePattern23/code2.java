import java.io.*;
class Pattern2 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter number of rows : ");
    int row = Integer.parseInt(br.readLine());
    int ch = row+96;
    for(int i= 1;i<=row;i++) {
      for(int j =1;j<=row;j++) {
        if(j>=(row-i+1)) {
	  System.out.print((char)(ch-32)+"\t");
	}else {
	  System.out.print((char)(ch)+"\t");
	}
	ch++;
      }
      System.out.println();
    }
  }
}
