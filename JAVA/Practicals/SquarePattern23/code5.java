
import java.io.*;
class Pattern5 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter number of rows : ");
    int row = Integer.parseInt(br.readLine());
    int num = row;
    for(int i =1;i<=row;i++) {
      for(int j =1;j<=row;j++) {
        if(i%2==1) {
	  if(j%2==0) {
	    System.out.print("$"+"\t");
	  }else {
	    System.out.print(num++ + "\t");
	  }
	}else{
	  if(num%2==1) {
	    num--;
	  }
	  System.out.print(num+"\t");
	}
      }
      System.out.println();
    }
  }
}
