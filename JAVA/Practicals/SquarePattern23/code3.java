import java.io.*;
class Pattern3 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter number of rows : ");
    int row = Integer.parseInt(br.readLine());
    int ch = row;
    for(int i= 1;i<=row;i++) {
      for(int j =1;j<=row;j++) {
        if((i+j)%2==0) {
	  System.out.print((char)(ch+96)+"\t");
	}else {
	  System.out.print(ch+"\t");
	}
	ch++;
      }
      System.out.println();
    }
  }
}
