import java.io.*;
class Pattern3 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter no. of rows : ");
    int row = Integer.parseInt(br.readLine());
    int col=0;
    for(int i =1;i<=2*row-1;i++){
      if(i<=row) {
         col = i;
      }else {
        col--;   // col = 2*row-i;
      }
      int num = col;
      for(int j =1;j<=col;j++) {
        System.out.print(num-- + "\t");
      }
      System.out.println();
    }
  }
}
