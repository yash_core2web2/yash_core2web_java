import java.io.*;
class Pattern10 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter no. of rows : "); 
    int row = Integer.parseInt(br.readLine());
    int col = 0;
    for(int i =1;i<=2*row-1;i++) {
      if(i<=row) {
        col = row-i;
      }else {
        col = i-row;
      }
      for(int j =1;j<=col;j++) {
        System.out.print("\t");
      }
      int num = col+65;
      if(i<=row) {
        col = i;
      }else {
        col = 2*row-i;
      }
      for(int k =1;k<=col;k++) {
        System.out.print((char)num+"\t");
	num++;
      }
      System.out.println();
    }
  }
}
