
class Array3 {
  public static void main(String[] args) {
    int arr[] = new int[]{2,5,2,8,9,2};
    int key = 2;
    int count = 0;
    for(int i =0;i<arr.length;i++) {
      if(key == arr[i]){
        count++;
      } 
    }
    if(count == 0) {
      System.out.println(key+" is not found");
    }else {
      System.out.println(key+" has occured "+count+" times");
    }
 } 
}
