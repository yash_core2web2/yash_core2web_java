
import java.io.*;
class Pattern10 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter no. of rows : ");
    int row = Integer.parseInt(br.readLine());
    for(int i =1;i<=row;i++) {
      for(int j =1;j<=row-i;j++) {
        System.out.print("\t");
      }
      int num = (row-i+1)+64;
      for(int k =1;k<=(2*i)-1;k++) {
        if(k<i) {
	  System.out.print((char)num + "\t");
	  num++;
	}else {
	  System.out.print((char)num + "\t");
	  num--;
	}
      }
      System.out.println();
    }
  }
}
