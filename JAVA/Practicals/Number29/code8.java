import java.io.*;
class Armstrong {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter number : ");
    int num = Integer.parseInt(br.readLine());
    int temp_1 = num;
    int count = 0;
    while(temp_1>0) {
      count++;
      temp_1 /= 10;
    }
    int temp_2 = num;
    int sum = 0;
    int rem = 0;
    if(num>=1 && num<=9) {
       System.out.println(num+" is a armstrong number");
    }else {
      while(temp_2>0) {
        rem = temp_2 %10;
        int prod = 1;
        for(int i =1;i<=count;i++) {
          prod *= rem;
        }
        sum += prod;
        temp_2 /= 10;
      }
      if(sum == num) {
        System.out.println(num+" is a armstrong number");
      }else {
        System.out.println(num+" is not a armstrong number");
      }
    }
  }
}
