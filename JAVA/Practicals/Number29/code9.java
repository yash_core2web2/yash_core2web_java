import java.io.*;
class Fibonacci {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
       System.out.print("Enter a number : ");
       int num = Integer.parseInt(br.readLine());
       int num1 = 0 ;
       int num2 = 1;
       int sum = 0;
       int i = 1;
       while(i<=num) {
          System.out.print(num1+"\t");
	  sum = num1 + num2;
	  num1 = num2;
	  num2 = sum;
	  i++;
       }
  }
}
