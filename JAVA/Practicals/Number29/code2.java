import java.io.*;
class Strong {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter number : ");
    int num = Integer.parseInt(br.readLine());
    int temp = num;
    int sum = 0;
    int rem = 0;
    while(temp>0) {
      rem = temp % 10;
      int fact = 1;
      for(int i =1;i<=rem;i++) {
        fact = fact*i;
      }
      sum = sum + fact;
      temp = temp/10;
    }
    if(sum == num) {
      System.out.println(num+" is a Strong number");
    }else{
      System.out.println(num+" is not a Strong number");
    }
  }
}
