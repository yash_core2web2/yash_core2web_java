import java.io.*;
class Automorphic {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter number : ");
    int num = Integer.parseInt(br.readLine());
    int sq = num*num;
    int temp_1 = num;
    int count = 0;
    while(temp_1 > 0) {
       count++;
       temp_1 /= 10;
    }
    int temp_2 = sq;
    int rev_1 = 0;
    int rem_1 = 0;
    for(int i =1;i<=count;i++) {
      rem_1 = temp_2 % 10;
      rev_1 = rev_1*10 + rem_1;
      temp_2 /= 10;
    }
    int rev_2 = 0;
    int rem_2 = 0;
    while(rev_1>0) {
      rem_2 = rev_1 % 10;
      rev_2 = rev_2*10 + rem_2;
      rev_1 /= 10;
    }
    if(num == rev_2) {
       System.out.println(num+" is a automorphic number");
    }else {
       System.out.println(num+" is not a automorphic number");
    }
  }
}
