import java.io.*;
class Duck {
  public static void main(String[] args)throws IOException {
     BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
     System.out.print("Enter element : ");
     int num = Integer.parseInt(br.readLine());
     int temp = num;
     int rem = 0;
     int flag = 0;
     while(temp>0) {
       rem = temp % 10;
       if(rem == 0) {
         flag = 1;
	 break;
       }
       temp /= 10;
     }
     if(flag == 1) {
       System.out.println(num+" is a duck number");
     }else {
       System.out.println(num+" is not a duck number");
     }
  }
}
