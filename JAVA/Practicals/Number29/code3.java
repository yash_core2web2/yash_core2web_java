import java.io.*;
class Deficient {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter number : ");
    int num = Integer.parseInt(br.readLine());
    int sum = 0;
    for(int i =1;i<num;i++) {
      if(num%i==0) {
        sum += i;
      }
    }
    if(sum < num) {
      System.out.println(num+" is a deficient number");
    }else{
      System.out.println(num+" is not a deficient number");
    }
  }
}
