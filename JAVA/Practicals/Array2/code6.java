import java.io.*;
class Array6 {
 public static void main(String[] args)throws IOException {	
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter size of array : ");
    int size = Integer.parseInt(br.readLine());
    char arr[] = new char[size];
    for(int i =0;i<size;i++) {
      System.out.print("Enter element at index "+i+" : ");
      arr[i] = (char)br.read();
      br.skip(1);
    }
    System.out.print("\nElements in array are : ");
    for(int i =0;i<size;i++) {
      System.out.print(arr[i]+",");
    }
 }
}
