import java.io.*;
class Array3 {
 public static void main(String[] args)throws IOException {	
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter size of array : ");
    int size = Integer.parseInt(br.readLine());
    int arr[] = new int[size];
    for(int i =0;i<size;i++) {
      System.out.print("Enter element at index "+i+" : ");
      arr[i] = Integer.parseInt(br.readLine());
    }
    for(int i =0;i<size;i++) {
     if(arr[i]%2==0) {
      System.out.println(arr[i]);
     }
    }
 }
}
