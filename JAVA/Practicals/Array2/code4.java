import java.io.*;
class Array4 {
 public static void main(String[] args)throws IOException {	
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter size of array : ");
    int size = Integer.parseInt(br.readLine());
    int arr[] = new int[size];
    for(int i =0;i<size;i++) {
      System.out.print("Enter element at index "+i+" : ");
      arr[i] = Integer.parseInt(br.readLine());
    }
    int sum = 0;
    for(int i =0;i<size;i++) {
      if(arr[i]%2 == 1) {
        sum = sum + arr[i];
      }
    }
    System.out.println("Sum of odd elements is "+sum);
 }
}
