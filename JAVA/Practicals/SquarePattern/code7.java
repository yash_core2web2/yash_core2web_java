import java.io.*;
class Pattern7{
 public static void main(String[] args)throws IOException {
   BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
   System.out.print("Enter no. of rows : ");
   int row = Integer.parseInt(br.readLine());
   int num = row;
   for(int i =1;i<=row;i++) {
     for(int j =1;j<=row;j++) {
       if(row%2==1) {
         if((i+j)%2==1) {
	   System.out.print(num + "\t");
	 }else {
	   System.out.print((char)(i+64) + "\t");
	 }
       }else {
         if(j%2==1) {
	   System.out.print(num + "\t");
	 }else {
	   System.out.print((char)(i+64) + "\t");
	 }
       }
       num++;
     }
     System.out.println();
   }
 }
}
