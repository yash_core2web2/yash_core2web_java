import java.io.*;
class Pattern1 {
  public static void main(String[] args)throws IOException {
     BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
     System.out.print("Enter no. of rows : ");
     int row = Integer.parseInt(br.readLine());
     for(int i =1;i<=row;i++) {
       for(int j =row;j>=1;j--) {
         if(i%2==0) {
	   System.out.print(row+"\t");
	 }else {
	   System.out.print((char)(j+64) + "\t");
	 }
       }
       System.out.println();
     }
  }
}
