
import java.io.*;
   class Pattern8 {
     public static void main(String[] args)throws IOException {
       BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
       System.out.print("Enter no. of rows : ");
       int row = Integer.parseInt(br.readLine());
     
       for(int i =1;i<=row;i++) {
	 for(int j=1;j<=i;j++) {
          if(j%2==0) {
	   System.out.print((char)(64+(j+((2*(i-2))+1)))+" ");
	  }else{
	   System.out.print(j+" ");
	  }
	 }
	 System.out.println();
       }
     }
   }
