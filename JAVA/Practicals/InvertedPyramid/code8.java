import java.io.*;
class Pattern8 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter number of rows : ");
    int row = Integer.parseInt(br.readLine());
    for(int i =1;i<=row;i++) {
      for(int j =1;j<i;j++) {
        System.out.print("\t");
      }
      int num = 1;
      for(int k =1;k<=2*(row-i+1)-1;k++) {
        if(k<(row-i+1)) {
	   System.out.print(num + "\t");
	   num++;
	}else {
	   System.out.print(num + "\t");
	   num--;
	}
      }
      System.out.println();
    }
  }
}
