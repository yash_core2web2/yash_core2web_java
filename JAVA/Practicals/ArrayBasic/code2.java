import java.io.*;
class Array2 {
 public static void main(String[] args)throws IOException {	
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter size of array : ");
    int size = Integer.parseInt(br.readLine());
    int arr[] = new int[size];
    for(int i =0;i<size;i++) {
      System.out.print("Enter element at index "+i+" : ");
      arr[i] = Integer.parseInt(br.readLine());
    }
    System.out.print("\nElements in array are : ");
    for(int i =0;i<size;i++) {
      System.out.print(arr[i]+",");
    }
 }
}
