import java.io.*;
class Array8 {
 public static void main(String[] args)throws IOException {	
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter no of employees : ");
    int count = Integer.parseInt(br.readLine());
    int arr[] = new int[count];
    for(int i =0;i<arr.length;i++) {
      System.out.print("Enter age of employee "+ (i+1) +" : ");
      arr[i] = Integer.parseInt(br.readLine());
    }
    for(int i =0;i<count;i++) {
      System.out.println("age of employee "+ (i+1) + " is "+arr[i]);
    }
 }
}
