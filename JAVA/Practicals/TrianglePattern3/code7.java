

import java.io.*;
class Pattern7 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter no. of rows : ");
    int row = Integer.parseInt(br.readLine());
    for(int i =1;i<=row;i++) {
	    int num = i;
      for(int j =row;j>=i;j--) {
        if(j%2==0) {
	  System.out.print(num + " ");
	}else {
	  System.out.print((char)(num+96) + " ");
	}
	num--;
      }
      System.out.println();
    }
  }
}
