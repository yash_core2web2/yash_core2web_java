


  class Demo {
    public static void main(String[] args) {
       long num = 9307922405l;
       long rem;
       long sum = 0;
       while(num>0) {
          rem = num%10;
	  sum = sum + rem;
	  num = num/10;
       }
       System.out.println("Sum of digits is : "+sum);
    }
  }
