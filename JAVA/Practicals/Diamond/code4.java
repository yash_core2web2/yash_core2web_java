import java.io.*;
class Pattern4 {
   public static void main(String[] args)throws IOException {
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      System.out.print("Enter no. of rows : ");
      int row = Integer.parseInt(br.readLine());
      int col = 0;
      int sp = 0;
      for(int i =1;i<=2*row-1;i++) {
         if(i<=row) {
	   sp = row-i;
	   col = 2*i-1;
	 }else {
	   sp = i-row;
	   col -= 2;
	 }
	 int num = 1;
	 for(int j =1;j<=sp;j++) {
	   System.out.print("\t");
	 }
	 for(int j =1;j<=col;j++) {
	   if(j<=col/2) {
	      System.out.print(num + "\t");
	      num++;
	   }else {
	      System.out.print(num + "\t");
	      num--;
	   }
	 }
	 System.out.println();
      }
   }
}
