import java.io.*;
class Array10 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("enter size of row : ");
    int row = Integer.parseInt(br.readLine());
    System.out.print("enter size of col : ");
    int col = Integer.parseInt(br.readLine());
    int arr[][] = new int[row][col];
    System.out.println("Enter elements of array : ");
    for(int i =0;i<row;i++) {
      for(int j =0;j<col;j++) {
         arr[i][j] = Integer.parseInt(br.readLine());
      }
    }
    for(int i =0;i<row;i++) {
      for(int j =0;j<col;j++) {
       if((i==0&&j==0)||(i==0&&j==col-1)||(j==0&&i==row-1)||(i==row-1&&j==col-1))
          System.out.print(arr[i][j]+"\t");
      }
    }
  }
}
