import java.io.*;
class Array9 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("enter size of row : ");
    int row = Integer.parseInt(br.readLine());
    System.out.print("enter size of col : ");
    int col = Integer.parseInt(br.readLine());
    int arr[][] = new int[row][col];
    int sum_pri = 0;
    int sum_sec = 0;
    System.out.println("Enter elements of array : ");
    for(int i =0;i<row;i++) {
      for(int j =0;j<col;j++) {
         arr[i][j] = Integer.parseInt(br.readLine());
	 if(i==j)
		 sum_pri += arr[i][j];
	 if(i+j==col-1)
                 sum_sec += arr[i][j];
      }
    }
    System.out.println(sum_pri);
    System.out.println(sum_sec);
    System.out.println("Product of Primary and secondary diag : "+(sum_pri*sum_sec));
  }
}
