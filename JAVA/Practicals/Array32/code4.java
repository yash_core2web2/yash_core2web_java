import java.io.*;
class Array4 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("enter size of row : ");
    int row = Integer.parseInt(br.readLine());
    System.out.print("enter size of col : ");
    int col = Integer.parseInt(br.readLine());
    int arr[][] = new int[row][col];
    System.out.println("Enter elements of array : ");
    for(int i =0;i<row;i++) {
      for(int j =0;j<col;j++) {
         arr[i][j] = Integer.parseInt(br.readLine());
      }
    }
    for(int i =0;i<row;i++) {
      int sum = 0;
      for(int j =0;j<col;j++) {
	if(i%2==0)
           sum += arr[i][j];
      }
      if(i%2==0)
        System.out.println("Sum of row "+(i+1)+" is : "+sum);
    }
  }
}
