

  class Demo {
    public static void main(String[] args) {
      int num = 9367924;
      int rem;
      int sum = 0;
      int prod = 1;
      while(num>0) {
        rem = num%10;
	if(rem%2==0) {
	  sum = sum + rem;
	}else {
	  prod = prod * rem;
	}
	num/=10;
      }
      System.out.println("Sum of even digit : "+sum);
      System.out.println("prod of odd digit : "+prod);
    }
  }
