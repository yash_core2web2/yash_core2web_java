
import java.io.*;
class Array5 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter size of array1 : ");
    int size1 = Integer.parseInt(br.readLine());
    int arr1[] = new int[size1];
    System.out.println("Enter elements of array1 : ");
    for(int i =0;i<size1;i++) {
      arr1[i] = Integer.parseInt(br.readLine());
    }
    System.out.print("Enter size of array2 : ");
    int size2 = Integer.parseInt(br.readLine());
    int arr2[] = new int[size2];
    System.out.println("Enter elements of array2 : ");
    for(int i =0;i<size2;i++) {
      arr2[i] = Integer.parseInt(br.readLine());
    }
    int size = size1 + size2;
    int arr[] = new int[size];
    int j = 0;
    int k = 0;
    for(int i =0;i<size;i++) {
      if(i<size1) {
	 arr[i] = arr1[j];
	 j++;
      }else {
         arr[i] = arr2[k];
	 k++;
      }
      System.out.print(arr[i]+" ");
    }
  }
}
