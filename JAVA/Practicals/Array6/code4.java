
import java.io.*;
class Array4 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter size of array : ");
    int size = Integer.parseInt(br.readLine());
    int arr1[] = new int[size];
    int arr2[] = new int[size];
    System.out.println("Enter elements of array : ");
    for(int i =0;i<size;i++) {
      System.out.println("Enter elements of array1 at index "+i+" : ");
      arr1[i] = Integer.parseInt(br.readLine());
      System.out.println("Enter elements of array2 at index "+i+" : ");
      arr2[i] = Integer.parseInt(br.readLine());
    }
    System.out.print("common elements are : ");
    for(int i =0;i<size;i++) {
      for(int j =0;j<size;j++) {
        if(arr1[i] == arr2[j]) {
	  System.out.print(arr1[i]+" ");
	}
      }
    }
  }
}
