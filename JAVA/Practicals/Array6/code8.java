import java.io.*;
class Array8 {
  public static void main(String[] args)throws IOException {
     BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
     System.out.print("Enter size of array : ");
     int size = Integer.parseInt(br.readLine());
     char arr[] = new char[size];
     System.out.println("Enter elements of array : ");
     for(int i =0;i<size;i++) {
        arr[i] = (char)br.read();
	br.skip(1);
     }
     System.out.println("Before Reverse : ");
     for(int i =0;i<size;i+=2) {
        System.out.print(arr[i]+" ");
     }
     System.out.println();
     for(int i =0;i<size/2;i++) {
        char temp = arr[i];
	arr[i] = arr[size-i-1];
	arr[size-i-1] = temp;
     }
     System.out.println("After Reverse : ");
     for(int i =0;i<size;i+=2) {
        System.out.print(arr[i]+" ");
     } 
  }
}
