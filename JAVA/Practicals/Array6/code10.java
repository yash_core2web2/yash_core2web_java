import java.io.*;
class Array10 {
  public static void main(String[] args)throws IOException {
     BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
     System.out.print("Enter size of array : ");
     int size = Integer.parseInt(br.readLine());
     int arr[] = new int[size];
     System.out.println("Enter elements of array : ");
     for(int i =0;i<size;i++) {
        arr[i] = Integer.parseInt(br.readLine());
     }
     int max_1 = arr[0];
     for(int i =0;i<size;i++) {
       if(max_1<arr[i]) {
         max_1 = arr[i];
       }
     }
     int max_2 = -1;
     if(max_1 == arr[0]) {
       max_2 = arr[1];
     }else {
       max_2 = arr[0];
     }
     for(int i =0;i<size;i++) {
       if(max_2<arr[i] && arr[i]<max_1) {
          max_2 = arr[i];
       }
     }
     int max_3 = -1;
     if((max_1==arr[0] && max_2==arr[1]) || (max_1==arr[1]&&max_2==arr[0])) {
        max_3 = arr[2];
     }else if(max_1==arr[0] || max_2==arr[0]) {
        max_3 = arr[1];
     }else {
        max_3 = arr[0];
     }
     for(int i =0;i<size;i++) {
	if(max_3<arr[i] && arr[i]<max_2) {
	   max_3 = arr[i];
	}
     }
     System.out.println("Third largest element is "+max_3);
  }
}
