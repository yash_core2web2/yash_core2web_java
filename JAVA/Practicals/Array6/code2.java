
import java.io.*;
class Array2 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter size of array : ");
    int size = Integer.parseInt(br.readLine());
    int arr[] = new int[size];
    System.out.println("Enter elements of array : ");
    for(int i =0;i<size;i++) {
      arr[i] = Integer.parseInt(br.readLine());
    }
    int sum = 0;
    int count_1 = 0;
    for(int i =0;i<size;i++) {
      int count_2 = 0;
      int j = 1;
      while(j<=arr[i]) {
        if(arr[i]%j==0) {
	  count_2++;
	}
	j++;
      }
      if(count_2==2) {
        sum = sum + arr[i];
	count_1++;
      }      
    }
    System.out.println("Sum : "+sum+" and Count : "+count_1);
  }
}
