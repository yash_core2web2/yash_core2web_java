import java.io.*;
class Array9 {
  public static void main(String[] args)throws IOException {
     BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
     System.out.print("Enter size of array : ");
     int size = Integer.parseInt(br.readLine());
     int arr[] = new int[size];
     System.out.println("Enter elements of array : ");
     for(int i =0;i<size;i++) {
        arr[i] = Integer.parseInt(br.readLine());
     }
     int count = 0;
     for(int i =0;i<size;i++) {
       int temp = arr[i];
       int rev = 0;
       int rem = 0;
       while(temp>0) {
         rem = temp % 10;
	 rev = rev*10 + rem;
	 temp /= 10;
       }
       if(rev == arr[i]) {
         count++;
       }
     }
     System.out.println("there are "+count+" palindrome elements");
  }
}
