import java.io.*;
class Array6 {
  public static void main(String[] args)throws IOException {
     BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
     System.out.print("Enter size of array : ");
     int size = Integer.parseInt(br.readLine());
     int arr[] = new int[size];
     System.out.println("Enter elements of array : ");
     for(int i =0;i<size;i++) {
        arr[i] = Integer.parseInt(br.readLine());
     }
     System.out.print("Enter search key : ");
     int key = Integer.parseInt(br.readLine());
     int count = 0;
     for(int i =0;i<size;i++) {
       if(arr[i]%key==0) {
         System.out.println("An element multiple found of "+key+" at index "+i);
	 count++;
       }
     }
     if(count == 0) {
       System.out.println("Element not found");
     }
    
  }
}
