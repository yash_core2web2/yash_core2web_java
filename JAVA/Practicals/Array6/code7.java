import java.io.*;
class Array7 {
  public static void main(String[] args)throws IOException {
     BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
     System.out.print("Enter size of array : ");
     int size = Integer.parseInt(br.readLine());
     int arr[] = new int[size];
     System.out.println("Enter elements of array : ");
     for(int i =0;i<size;i++) {
	 arr[i] = Integer.parseInt(br.readLine());
     }
     for(int i =0;i<size;i++) {
        if(arr[i]>='A' && arr[i]<='Z') {
	  System.out.print((char)arr[i]+" ");
	}else {
	  System.out.print(arr[i]+" ");
	}
     }
  }
}
