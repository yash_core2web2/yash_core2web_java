
import java.io.*;
class Array3 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter size of array : ");
    int size = Integer.parseInt(br.readLine());
    int arr[] = new int[size];
    System.out.println("Enter elements of array : ");
    for(int i =0;i<size;i++) {
      arr[i] = Integer.parseInt(br.readLine());
    }
    System.out.print("Enter key : ");
    int key = Integer.parseInt(br.readLine());
    int count = 0;
    for(int i =0;i<size;i++) {
      if(arr[i]==key) {
	  count++;
      }
    }
    if(count > 2) {
        for(int i =0;i<size;i++) {
	  if(arr[i]==key) {
	    arr[i] = key*key*key;
	  }
	  System.out.print(arr[i]+" ");
	}
    }
  }
}
