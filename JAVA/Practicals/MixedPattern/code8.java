

import java.io.*;
class Pattern8 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter no. of rows : ");
    int row = Integer.parseInt(br.readLine());
    int num;
    if(row%2==0) {
      num = (row*row/2)+row/2;
    }else {
      num = row*(row+1)/2;
    }
    for(int i =1;i<=row;i++) {
      for(int j =1;j<=row-i+1;j++) {
         System.out.print((char)(num+64) + " ");
	 num--;
      }
      System.out.println();
    }
  }
}
