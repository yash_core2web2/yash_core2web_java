
import java.io.*;
class Demo {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter no. : ");
    long num = Long.parseLong(br.readLine());
    long temp = num;
    long rem;
    long rev = 0;
    while(temp>0) {
      rem = temp%10;
      rev = rev*10 + rem;
      temp /=10;
    }
    while(rev>0) {
      rem = rev%10;
      if(rem%2==1) {
        System.out.print(rem*rem + ",");
      }
      rev /=10;
    }
 
  }
}
