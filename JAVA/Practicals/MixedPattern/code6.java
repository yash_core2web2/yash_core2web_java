

import java.io.*;
class Pattern6 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter no. of rows : ");
    int row = Integer.parseInt(br.readLine());
    for(int i =1;i<=row;i++) {
      for(int j =row;j>=row-i+1;j--) {
        if(i%2==0) {
	  System.out.print(j+" ");
	}else {
	  System.out.print((char)(j+96) + " ");
	}
      }
      System.out.println();
    }
  }
}
