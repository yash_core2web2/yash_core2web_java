

import java.io.*;
class Pattern9 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter no. of rows : ");
    int row = Integer.parseInt(br.readLine());
    for(int i =1;i<=row;i++) {
      int num = (row-i+1)+64;
      for(int j =1;j<=row-i+1;j++) {
         if(i%2==1) {
	   System.out.print(j+" ");
	 }else {
	   System.out.print((char)(num)+" ");
	   num--;
	 }
      }
      System.out.println();
    }
  }
}
