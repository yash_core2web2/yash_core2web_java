import java.io.*;
class Pattern5 {
  public static void main(String[] args)throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print("Enter no. of rows : ");
    int row = Integer.parseInt(br.readLine());
    int num  = 3;
    for(int i =1;i<=row;i++) {
      for(int k=1;k<=row;k++) {
        if(i%2==0) {
           System.out.print(num + "\t");
	}else {
           System.out.print(num*num + "\t");
	}
	num++;
      }
      System.out.println();
    }
  }
}
