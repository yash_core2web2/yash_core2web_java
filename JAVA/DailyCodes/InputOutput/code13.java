import java.util.*; 

class ScannerDemo {
  public static void main(String[] args) {
    System.out.print("Enter name : ");
    Scanner sc = new Scanner(System.in);
    String name = sc.next();
    System.out.println("name is : "+name);
  }
}

// next(); method reads String

// (System.in) connects keyboard to our program which is running under JVM

 
