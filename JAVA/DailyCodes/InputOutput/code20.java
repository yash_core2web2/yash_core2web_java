


import java.io.*;

  class Demo {
    public static void main(String[] args)throws IOException {
      InputStreamReader isr = new InputStreamReader(System.in);

      int x = isr.read();          //isr.read() only returns integer value.
      System.out.println(x);
    }
  }

// try giving inputs as String, float ,long etc
