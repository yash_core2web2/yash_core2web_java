
// Diidference betweeen next() and nextLine()

import java.util.*;
class Demo {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.print("Enter name : ");
    String name = sc.nextLine();      
    System.out.print(name);
  }
}

//  Enter input as : yash ashok patil
//  nextLine() reads full line.
