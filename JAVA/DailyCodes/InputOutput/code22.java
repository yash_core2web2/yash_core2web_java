

  import java.io.*;

  class Demo {
    public static void main(String[] args)throws IOException {
      InputStreamReader isr = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader(isr);

      System.out.print("Enter name : ");
      String name = br.readLine();  
      System.out.println(name);
    }
  
 }


// br.readLine();  converts all inputs to String
