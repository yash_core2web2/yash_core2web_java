  // Bufferred Reader Method :-
  //
  

  import java.io.*;

  class Demo {
    public static void main(String[] args) {
      InputStreamReader isr = new InputStreamReader(System.in);
      //int x = isr.read();                            // this read(); method used to read one char in  InputStreamReader as it can take only  one char at a time.
    }
  }

   // Here there is error becoz we have to throw or try-catch a exception if pipeline is broke which takes input.
  // this error only comes when we try to use read or any methods from InputStreamreader or BufferedREader.
