

class PrintDemo {
   public static void main(String[] args) {
     System.out.println("Hello 1 ");
     System.err.println("Hello 2 ");  // err is also object of PrintStream class hence it can also access method - println(); as it is method from class PrintStream.
   }
}
