

  import java.io.*;

  class Demo {
    public static void main(String[] args) {
      InputStreamReader isr = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader(isr);
      String name = br.readLine();  // It also need Exception handling.
      System.out.println(name);
    }
  
 }


// br.readLine();  converts all inputs to String
