//This is how you get char as a user input

import java.util.*;
class CharDemo {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.print("Enter character : ");
    char ch = sc.next().charAt(0);
    System.out.print("character : "+ch);

  }
}
