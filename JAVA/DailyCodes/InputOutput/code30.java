

import java.io.*;
class CharDemo {
  public static void main(String[] args)throws IOException {
     BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
     System.out.print("Enter name : ");
     String name = br.readLine();
     System.out.print("Enter Society name : ");
     String socName = br.readLine();
     System.out.print("Enter wing : ");
     char wing = (char)br.read();
     br.skip(1);     
     System.out.print("Enter Flat no. : ");
     int num = Integer.parseInt(br.readLine());

     System.out.println("\nname : "+name);
     System.out.println("Socname : "+socName);
     System.out.println("wing : "+wing);
     System.out.println("flat no : "+num);

  }
}
