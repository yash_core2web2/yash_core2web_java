

import java.io.*;
class CharDemo {
  public static void main(String[] args)throws IOException {
     BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
     System.out.print("Enter name : ");
     String name = br.readLine();
     System.out.print("Enter Flat no. : ");
     int num = Integer.parseInt(br.readLine());
     System.out.print("Enter wing : ");
     char wing = (char)br.read();                //here /n is stored in the buffer.Which id read bu int num line so it gives error
     System.out.print("Enter Society name : ");
     String socName = br.readLine();
     
     System.out.println("name : "+name);
     System.out.println("flat no : "+num);
     System.out.println("wing : "+wing);
     System.out.println("Socname : "+socName);

  }
}
