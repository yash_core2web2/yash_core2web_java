

class ScannerDemo {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String name = sc.next();
    System.out.println(name);
  }
}

  //  It gives 2 errors becoz left side Scanner is reference and right side Scanner is Class.
