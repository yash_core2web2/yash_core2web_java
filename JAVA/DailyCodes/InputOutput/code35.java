//  StingTokenizer

import java.util.*;
class StringTokenizerDemo {
  public static void main(String[] args) {
    Scanner sc = new  Scanner(System.in);
    System.out.print("Enter string : ");
    String jerNo = sc.nextLine();
    StringTokenizer st = new StringTokenizer(jerNo," ");  //  " " called as delimiter.
    String str1 = st.nextToken();
    System.out.println(str1);
    String str2 = st.nextToken();
    System.out.println(str2);
    String str3 = st.nextToken();
    System.out.println(str3);

    

  }
}

// Input1 =  18 45 7
// Input2 =  18 45  
// Input3 =  18 45 7 8
