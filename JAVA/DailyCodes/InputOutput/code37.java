//  StingTokenizer

import java.util.*;
class StringTokenizerDemo {
  public static void main(String[] args) {
    Scanner sc = new  Scanner(System.in);
    System.out.print("Enter string : ");
    String jerNo = sc.nextLine();
    StringTokenizer st = new StringTokenizer(jerNo," ");  
    while(st.hasMoreTokens()) {
	   System.out.println(st.nextToken()); 
    }

  }
}
// Input = 18 1 45
// Input = 18,12,45
