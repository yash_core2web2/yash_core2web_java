
  class InputDemo {
      static void fun() {
        System.out.println("In fun");
      }
      public static void main(String[] args) {
        System.out.println("In main");
	fun();
	InputDemo.fun();  // we can call it using class name but its unnecessary as Both statics are within same class.
      }
    }
  


    // We can directly call only static methods and variables from static method.
