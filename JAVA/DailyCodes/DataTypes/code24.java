


   class Initialisation {
      public static void main(String[] args) {
          int x;
	  System.out.println(x);
      }
   }



    // Java does not support declaration of variable.
   // We have to initialize variable or assign immediately after declaration
   //
   // This code compiles because we havent used x variable further in the code But if we try to print it or do operations then it will give error
