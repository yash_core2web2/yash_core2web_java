



class Bitwise{
	public static void main(String[] args){
		int x = 10; // binary = 0000 1010
		int y = 12; // binary = 0000 1100
		System.out.println(x|y); // 14 i.e.0000 1110


		/*
		 here the x | y means 

		 00001010
		 |
		 00001100
		 */
	}
}


/*
 * This uses OR truth table.
 * Moving from Left to Right ,, 0 & 0 is 0.
                                0 & 1 is 1.
				1 & 0 is 1.
				1 & 1 is 1.
*/


