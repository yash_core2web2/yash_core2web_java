


  class NumberSystem {
     public static void main(String[] args) {
        int x = 0b00001010;  //this is to convert binary to int ie 0b at start means binary
	int y = 0xa;         //this is to convert hexadecimal to integer ir 0x at start means hexadecimal
	int z = 012;         //this is to convert octal to integer ie 0 at start means ocatal

	System.out.println(x);
	System.out.println(y);
	System.out.println(z);
     }
  }
