

 import java.util.*;
class Demo {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.print("enter array size : ");
    int size = sc.nextInt();
    int arr[] = new int[size];
    System.out.print("array size is : "+arr.length);
    for(int i=0;i<size;i++) {
      System.out.print("enter element : ");
      arr[i] = sc.nextInt();
    }
    for(int i=0;i<size;i++) {
      System.out.println(arr[i]);    
    }
    int sum = 0;
    for(int i=0;i<size;i++) {
      sum = sum+arr[i];  
    }
      System.out.println("Sum of array elements is : "+sum);    
  }
}
