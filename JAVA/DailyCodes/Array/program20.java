
class Demo {
  public static void main(String[] args) {
    int arr[] = new int[3]{105,207,309};
    System.out.println(arr[0]);
    System.out.println(arr[1]);
    System.out.println(arr[2]);

    arr[0] = 10;
    arr[1] = 20;
    arr[2] = 30;

    System.out.println(arr[0]);
    System.out.println(arr[1]);
    System.out.println(arr[2]);
  }
}
