
// This is another method
  class Demo {
    public static void main(String[] args) {
      int arr[][] = new int[3][3]{{10,20,30},{30,40},{50}};   //It gives comp time error if u give any dimension
      System.out.println(arr[0][0]); 
      System.out.println(arr[0][1]); 
      System.out.println(arr[0][2]); 
      System.out.println(arr[1][0]); 
      System.out.println(arr[1][1]); 
      System.out.println(arr[1][2]); 
    }
  }


/*       10 20 30
 *       30 40
 *       50
                   this will be visual representation of above array.
*/
