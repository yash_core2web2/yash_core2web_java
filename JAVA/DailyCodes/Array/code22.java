
// This is another method
  class Demo {
    public static void main(String[] args) {
      int arr[][] = new int[][]{{10},20,{30,40},50}; //Here comp error becoz every element in array must be in curly brace to represent its row 
      System.out.println(arr[0][0]);                  // So list shoul be {{10},{20},{30,40},{50}} 
      System.out.println(arr[0][1]); 
      System.out.println(arr[0][2]); 
      System.out.println(arr[1][0]); 
      System.out.println(arr[1][1]); 
      System.out.println(arr[1][2]); 
    }
  }


/*       10 
 *       20
 *       30 40
 *       50
                   this will be visual representation of above array.
*/
