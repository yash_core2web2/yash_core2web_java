


  class SwitchDemo {
    public static void main(String[] args) {
        int a = 5;
	System.out.println("Before Switch");
	switch(a) {
	    case 1 :
		    System.out.println("one");
		    break;
	    case 2 :
		    System.out.println("two");
		    break;
	    case 3 :
		    System.out.println("three");
		    break;
	    /*default :
		    System.out.println("Default state");
		    break;*/
	}
	System.out.println("After Switch");
    }
  }
