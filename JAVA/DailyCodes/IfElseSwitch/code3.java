


  class SwitchDemo {
    public static void main(String[] args) {
        int a = 2;
	switch(a) {
	    case 1 :
		    System.out.println("one");
		    break;
	    case 2 :
		    System.out.println("two");
		    break;
	    case 3 :
		    System.out.println("three");
		    break;
	    default :
		    System.out.println("Default state");
		    break;
	}
    }
  }
    // Switch supports int ,char, string But does not support double,float  boolean.  

   // if we dont write break then it will print two three Default state.
