


  class SwitchDemo {
    public static void main(String[] args) {
        int data = 'A';
	System.out.println("Before Switch");
	switch(data) {
	    case 'A' :
		    System.out.println("one");
		    break;
	    case 'B' :
		    System.out.println("two");
		    break;
	    case 'C' :
		    System.out.println("three");
		    break;
	    default :
		    System.out.println("Default state");
		    break;
	}
	System.out.println("After Switch");
    }
  }
