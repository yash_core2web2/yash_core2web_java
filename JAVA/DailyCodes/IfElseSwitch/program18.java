


  class SwitchDemo {
      public static void main(String[] args) {
        String num = "One";
	System.out.println("Before Switch");
        switch(num){
	  case "One" :
	      System.out.println("One");
	      break;
	
	  case "Two" :
	      System.out.println("Two");
	      break;
	
	  case "Three" :
	      System.out.println("Three");
	      break;
	
	  default :
	      System.out.println("Default");
	
      }
  
      } 
}
