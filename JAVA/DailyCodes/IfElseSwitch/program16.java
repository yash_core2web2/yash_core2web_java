


  class SwitchDemo {
      public static void main(String[] args) {
        char num = 'A';
	System.out.println("Before Switch");
        switch(num){
	  case 'A' :
	      System.out.println("One");
	      break;
	
	  case 'B' :
	      System.out.println("Two");
	      break;
	
	  case 'C' :
	      System.out.println("Three");
	      break;
	
	  default :
	      System.out.println("Default");
	
      }
  
      } 
}
