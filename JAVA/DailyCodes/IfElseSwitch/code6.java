


  class SwitchDemo {
    public static void main(String[] args) {
        String data = "Thala";
	System.out.println("Before Switch");
	switch(data) {
            case "Koach" :
		    System.out.println("18");
		    break;
	    case "Vadapao" :
		    System.out.println("45");
		    break;
	    case "Thala" :
		    System.out.println("7");
		    break;
	    default :
		    System.out.println("Default state");
		    break;
	}
	System.out.println("After Switch");
    }
  }


   // switch can take integer , char , String
