


   class Demo {
     public static void main(String[] args) {
	for(int i =1;i<=5;i++) {
		switch(i) {
		  case 1 :
			  System.out.println("one");
			  break;
		  case i :
			  System.out.println("Two");
			  break;
	          
		}
	
	}
     }
   }


//In this switch statement, case i: attempts to use the variable i as the case label. However, the case labels in a switch statement must be compile-time constants. This means they should be known to the compiler when it's translating your code into bytecode.
