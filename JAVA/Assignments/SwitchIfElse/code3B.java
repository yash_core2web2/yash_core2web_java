

  class Size {
    public static void main(String[] args) {
       String size = "XL";
       if(size=="S") {
          System.out.println("Small");
       }
       else if(size=="M") {
          System.out.println("Medium");
       }
       else if(size=="L") {
          System.out.println("Large");
       }
       else if(size=="XL") {
          System.out.println("Extra Large");
       }
       else{
          System.out.println("Invalid Size");
       }
    }
  }
